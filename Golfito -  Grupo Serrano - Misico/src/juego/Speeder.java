package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Speeder {
	private int x, y;
	private int ancho;
	private int alto;
	private Image img;
	public Speeder(int x, int y, int ancho, int alto){
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo(this.x, this.y, this.ancho, this.alto,0, Color.blue);		
		Image f = Herramientas.cargarImagen("Speeder.png");
		entorno.dibujarImagen(f, this.x, this.y, 0, 1);
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public int getAlto() {
		return this.alto;
	}
	
	public int getAncho() {
		return this.ancho;
	}

	
	boolean pelotaEnSpeeder(Pelotita p){
		return this.x + 10 >= p.getX() - p.getDiametro()/2 && this.x - 10 <= p.getX() + p.getDiametro()/2 && this.y -10 < p.getY() + p.getDiametro()/2 && this.y + 10 >= p.getY() - p.getDiametro()/2;
	}
	
	
}

