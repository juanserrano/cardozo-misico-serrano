package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Potenciador {
	private double x;
	private double y;
	private Image img;
	
	Potenciador(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	void dibujar(Entorno e){
		e.dibujarRectangulo(this.x, this.y, 150, 20, 0, Color.BLACK);
		Image f = Herramientas.cargarImagen("Potenciador.png");
		e.dibujarImagen(f, this.x, this.y, 0, 1);
	}
}

