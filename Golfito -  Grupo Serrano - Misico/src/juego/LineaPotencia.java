package juego;

import java.awt.Color;

import entorno.Entorno;

public class LineaPotencia {
	private double x;
	private double y;
	
	LineaPotencia(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	void dibujar(Entorno e){
		e.dibujarRectangulo(this.x, this.y, 10, 30, 0, Color.WHITE);
	}

	void subirFuerza(){
		this.x+=1;
	}

	void disminuirFuerza(){
		this.x=this.x-2;
	}

	public double getX(){
		return this.x;
	}
	
	public double getY(){
		return this.y;
	}

	public void reset(){
		this.x=20;
		this.y=20;
	}













}
