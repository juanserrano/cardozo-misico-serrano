package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Agua {
	// Variables de instancia
	private int x, y;
	private int ancho;
	private int alto;
	private Image img;
	public Agua(int x, int y, int ancho, int alto){
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.blue);		
		Image f = Herramientas.cargarImagen("Agua.png");
		entorno.dibujarImagen(f, this.x, this.y, 0, 1);
	}
	
	public int getX(){
		return this.x;
	}
	
	public int getY(){
		return this.y;
	}
	
	public int getalto(){
		return this.alto;
	}
	
	public int getancho(){
		return this.ancho;
	}

	
	boolean pelotaEnAgua(Pelotita p){
		return this.x + 100 >= p.getX() - p.getDiametro()/2 && this.x - 100 <= p.getX() + p.getDiametro()/2 && this.y -100 < p.getY() + p.getDiametro()/2 && this.y + 100 >= p.getY() - p.getDiametro()/2;
	}
	
}

