package juego;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.color.*;

import entorno.Entorno;
import entorno.Herramientas;

public class Flecha {
	// Variables de instancia
	private double x, y;
	private int ancho;
	private int alto;
	private double angulo;
	private Image img;

	public Flecha(double x, double y){
		this.x = x;
		this.y = y;
		this.ancho = 1;
		this.alto = 1;
		this.angulo = 0;
		img= Herramientas.cargarImagen("Flecha.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, angulo, Color.red);
		Image f = Herramientas.cargarImagen("Flecha.png");
		entorno.dibujarImagen(f, this.x, this.y, angulo);
	}
	
	public void girarAntiHorario(){
		this.angulo+=0.05;
	}
	
	public void girarHorario(){
		this.angulo-=0.05;
	}
	
	public double getX(){
		return this.x;
	}

	public double getAngulo(){
		return this.angulo;
	}
	
	public double getY(){
		return this.y;
	}
	
	public void posicionateEn(double x, double y){
		this.x=x;
		this.y=y;
	}
	
}