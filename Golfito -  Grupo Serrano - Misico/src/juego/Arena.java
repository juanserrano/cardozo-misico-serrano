package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Arena {
	private int x,y;
	private double diametro;
	private Image img;
	
	public Arena(int x , int y ,double diametro){
		this.x=x;
		this.y=y;
		this.diametro=diametro;
	}

	public void dibujar(Entorno entorno){
		entorno.dibujarCirculo(this.x,this.y,this.diametro, Color.yellow);		
		Image f = Herramientas.cargarImagen("Arena.png");
		entorno.dibujarImagen(f, this.x, this.y, 0, 1);
	}

	public int getY(){
		return this.y;
	}

	public int getX(){
		return this.x;
	}

	boolean pelotaEnArena(Pelotita p){
		return this.x + 100 >= p.getX() - p.getDiametro()/2 && this.x - 100 <= p.getX() + p.getDiametro()/2 && this.y - 100 < p.getY() + p.getDiametro()/2 && this.y + 100 >= p.getY() - p.getDiametro()/2;
	}
	
}