
package juego;

import java.awt.Color;
import java.awt.Image;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Potenciador potenciador;
	private Pelotita pelotita;
	private Hoyo hoyo;
	private Image fondo;
	private Flecha flecha;
	private Arena arena;
	private Agua agua;
	private Speeder speeder;
	private LineaPotencia lineapotencia;
	private boolean disparar;
	private int golpes;
	private double guardarYpelotita;
	private double guardarXpelotita;
	private Image win;
	private Image fotoPelota;
	private Image fotoFlecha;
	private Image fotoHoyo;
	private Image fotoAgua;
	private Image fotoArena;
	private Image fotoSpeeder;
	private Image fotoWin;
	private Image fotoPotenciador;

	// Variables y métodos propios de cada grupo
	// ...

	Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(
				this,
				"Golfito - Grupo Serrano - Misico - V0.01",
				800, 600);

		// Inicializar lo que haga falta para el juego
		// ...
		this.pelotita = new Pelotita(100, 100, 25,Math.PI / 3);
		this.flecha = new Flecha(this.pelotita.getX(),this.pelotita.getY());
		this.win = Herramientas.cargarImagen("Win.gif");
		this.fondo = Herramientas.cargarImagen("descarga.jpg");
		this.fotoPelota= Herramientas.cargarImagen("Pelota.png");
		this.fotoFlecha= Herramientas.cargarImagen("Flecha.png");
		this.fotoHoyo= Herramientas.cargarImagen("Hoyo.png");
		this.fotoAgua= Herramientas.cargarImagen("Agua.png");
		this.fotoArena= Herramientas.cargarImagen("Arena.png");
		this.fotoPotenciador= Herramientas.cargarImagen("Potenciador.png");
		this.potenciador = new Potenciador(100,20);
		this.lineapotencia = new LineaPotencia(20,20);
		this.hoyo = new Hoyo(600,550,35);
		this.speeder = new Speeder(600,100,20,20);
		
		Random aleatorio = new Random();
		Random ale = new Random();
		int xAgua = 300 + aleatorio.nextInt(200);
		int yAgua = 300 + ale.nextInt(150);
		this.agua = new Agua(xAgua, yAgua,200,200);
		Random r = new Random();
		Random ran = new Random();
		int a = 100 + r.nextInt(600);
		int b = 100 + ran.nextInt(400);
		this.arena = new Arena(a,b, 200);
		
		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y por
	 * lo tanto es el método más importante de esta clase. Aquí se debe
	 * actualizar el estado interno del juego para simular el paso del tiempo
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo
		// ...
		this.entorno.dibujarImagen(fondo, 400, 300, 0, 1);
		this.arena.dibujar(this.entorno);
		this.hoyo.dibujar(this.entorno);
		this.agua.dibujar(this.entorno);
		this.speeder.dibujar(entorno);
		this.potenciador.dibujar(this.entorno);
		this.lineapotencia.dibujar(this.entorno);
		if (this.pelotita.estaQuieta() && !pelotita.enHoyo(hoyo)){
			this.flecha.dibujar(this.entorno);
			disparar=false;
		}
		this.pelotita.dibujar(this.entorno);
		String titulo= "Golpes: " + this.golpes;
		this.entorno.cambiarFont(titulo, 40, Color.RED);
		this.entorno.escribirTexto(titulo, 550, 40);
		

		
		if (entorno.estaPresionada(entorno.TECLA_ESPACIO)&& this.lineapotencia.getX()<170 && disparar == false){
			this.lineapotencia.subirFuerza();
			this.pelotita.cargaPotencia(entorno);
			}

		if (entorno.estaPresionada(entorno.TECLA_DERECHA)) {
			this.flecha.girarAntiHorario();
		}
	

		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			this.flecha.girarHorario();
		}
			
		if (entorno.estaPresionada(entorno.TECLA_ENTER)){
			this.disparar=true;
			this.pelotita.getAngulo();
			if (entorno.sePresiono(entorno.TECLA_ENTER) && guardarXpelotita==this.pelotita.getX() && guardarYpelotita==this.pelotita.getY()){
				this.golpes++;
			}	
		}
	
		if(disparar){
			this.pelotita.mover();
			this.pelotita.decrementar();	
		}
		
		if (this.pelotita.getVelocidad() == 0){
			this.lineapotencia.reset();
			guardarXpelotita=this.pelotita.getX();
			guardarYpelotita=this.pelotita.getY();
		}
		
		if (this.pelotita.tocaPared(this.entorno)){
			this.pelotita.aumentarVelocidad();
			this.pelotita.cambiarAngulo();
			
		}
		
		this.pelotita.cambiarAnguloPorFlecha(this.flecha);
		
		this.flecha.posicionateEn(this.pelotita.getX(), this.pelotita.getY());
	
		if (this.arena.pelotaEnArena(pelotita)){
			this.pelotita.decrementarVelocidadArena();
		}

		//En esta parte la pelotita esta dentro del hoyo y se "gana". Contiene opcion de reiniciar el juego.
		if (this.pelotita.enHoyo(hoyo)){
		this.pelotita.frenar();
		this.pelotita.posicionateEn(this.hoyo.getX(), this.hoyo.getY());
		this.entorno.dibujarImagen(win, 400, 300, 0, 3);
		String replay= "Presione la tecla SHIFT para volver a jugar.";
		this.entorno.cambiarFont(replay, 25, Color.YELLOW);
		this.entorno.escribirTexto(replay, 70, 80);
		if (entorno.estaPresionada(entorno.TECLA_SHIFT)){
			this.pelotita.posicionateEn(100, 100);
			this.golpes=0;
			}
		}
		
		if(this.agua.pelotaEnAgua(pelotita)) {
			this.pelotita.frenar();
			this.pelotita.posicionateEn(guardarXpelotita, guardarYpelotita);
			this.golpes++;
		}

		if(this.speeder.pelotaEnSpeeder(pelotita)) {
			this.pelotita.speedUp();
		}
	}

	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}