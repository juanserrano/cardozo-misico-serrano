package juego;

import java.awt.Color;

import entorno.Entorno;
import entorno.Herramientas;
import juego.Flecha;

import java.awt.Image;

public class Pelotita {
	 private double x;
	 private double y;
	 private int diametro;
	 private double angulo;
	 private double velocidad;
	 private Image img;
	 
	public Pelotita(double x, double y, int diametro, double angulo) {
		this.x = x;
		this.y = y;
		this.diametro = diametro;
		this.angulo = angulo;
		this.velocidad = 0;
		img= Herramientas.cargarImagen("Pelota.png");
	}
	
	public void mover(){
		this.x = this.x + (this.velocidad * Math.cos(this.angulo));
		this.y = this.y + (this.velocidad * Math.sin(this.angulo));
	}
	
	public void cambiarAngulo(){
		this.angulo = this.angulo + Math.PI/2;
	}
	
	public void aumentarVelocidad(){
		this.velocidad = velocidad+0.01;
	}
	
	void dibujar(Entorno e){
		e.dibujarCirculo(this.x, this.y,this.diametro, Color.WHITE);
		Image f = Herramientas.cargarImagen("Pelota.png");
		e.dibujarImagen(f, this.x, this.y, angulo);
	}
	
	public double getX(){
		return this.x;
	}

	public double getAngulo(){
		return this.angulo;
	}
	
	public double getY(){
		return this.y;
	}
	
	public double getDiametro(){
		return this.diametro;
	}
	
	public double getVelocidad(){
		return this.velocidad;
	}
	
	boolean tocaPared(Entorno e){
		return this.y > e.alto() - this.diametro/2 || this.x > e.ancho() - this.diametro/2 || this.y < 0 + this.diametro/2 || this.x < 0 + this.diametro/2;
	}
	
	boolean estaEnMovimiento(){
		return this.velocidad>0;	
	}
	
	boolean estaQuieta(){
		return this.velocidad<=0;
	}

	void cambiarAnguloPorFlecha(Flecha f){
		if (estaQuieta() && this.angulo != f.getAngulo()){
			this.angulo = f.getAngulo();
		}
	}

	public void decrementar(){
		if(estaEnMovimiento()&&velocidad>0){
			this.velocidad = this.velocidad - 0.01;
		if(velocidad<0){
			velocidad=0;
			}
		}
	}

	public void cargaPotencia(Entorno e){
		if (e.estaPresionada(e.TECLA_ESPACIO)){
			this.velocidad+=0.05;
		}
	}

	boolean enHoyo(Hoyo hoyo){
		return this.velocidad<4 && this.x > hoyo.getX() - hoyo.getDiametro()/2 && this.x < hoyo.getX() + hoyo.getDiametro()/2 && this.y < hoyo.getY() + hoyo.getDiametro()/2 && this.y > hoyo.getY() - hoyo.getDiametro()/2;
	}
	
	public void posicionateEn(double x, double y){
		this.x=x;
		this.y=y;
	}

	public void decrementarVelocidadArena(){
		if (estaEnMovimiento()&&this.velocidad>0){
			this.velocidad=this.velocidad - 0.03;
			if(velocidad<0){
				velocidad=0;
			}
		}	
	}
	
	public void frenar(){
		this.velocidad=0;
	}

	public void speedUp(){
		this.velocidad+=0.15;
	}
}