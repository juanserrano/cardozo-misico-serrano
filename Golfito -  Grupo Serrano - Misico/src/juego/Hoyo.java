package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Hoyo {
	private double x;
	private double y;
	private double diametro;
	private Image img;
	
	public Hoyo(double x, double y, double diametro) {
		this.x = x;
		this.y = y;
		this.diametro = diametro;
		img= Herramientas.cargarImagen("Hoyo.png");
	}

	public double getX(){
		return x;
	}

	public double getY(){
		return y;
	}

	public double getDiametro(){
		return diametro;
	}

	void dibujar(Entorno e){
		e.dibujarCirculo(this.x, this.y, this.diametro, Color.BLACK);
		Image f = Herramientas.cargarImagen("Hoyo.png");
		e.dibujarImagen(f, this.x, this.y, 0, 1);
	}
	

	
}